document.addEventListener("DOMContentLoaded", function () {
  // Определение класса Visit для создания объектов, представляющих информацию о визите
  class Visit {
    constructor(name, doctor, purpose, description, urgency, status) {
      this._name = name;
      this._doctor = doctor;
      this._purpose = purpose;
      this._description = description;
      this._urgency = urgency;
      this._status = status;
    }
  }

  class VisitDentist extends Visit {
    constructor(name, purpose, description, urgency, status, lastVisit) {
      super(name, 'Dentist', purpose, description, urgency, status);
      this._lastVisit = lastVisit;
    }
  }
  
  class VisitCardiologist extends Visit {
    constructor(name, purpose, description, urgency, status, pressure, bmi, heartDiseases, age) {
      super(name, 'Cardiologist', purpose, description, urgency, status);
      this._pressure = pressure;
      this._bmi = bmi;
      this._heartDiseases = heartDiseases;
      this._age = age;
    }
  }
  
  class VisitTherapist extends Visit {
    constructor(name, purpose, description, urgency, status, age) {
      super(name, 'Therapist', purpose, description, urgency, status);
      this._age = age;
    }
  }

  // Класс Modal для работы с модальными окнами
  class Modal {
    constructor(modalId, closeButtonId) {
      this.modal = document.getElementById(modalId);
      this.closeButton = document.getElementById(closeButtonId);
      this.closeButton.addEventListener('click', this.closeModal.bind(this));
    }
    openModal() {
      this.modal.style.display = 'block';
    }
    closeModal() {
      const additionalFieldsContainer = document.getElementById('additionalFieldsContainer');
      additionalFieldsContainer.innerHTML = '';
      this.modal.style.display = 'none';
      const inputFields = this.modal.querySelectorAll('input');
      inputFields.forEach(input => {
        input.value = '';
      });
      const selectFields = this.modal.querySelectorAll('select');
      selectFields.forEach(select => {
        select.selectedIndex = 0;
      });
    }
    openEditModal(visit) {
      const doctorSelect = document.getElementById('doctorSelect');
      const nameInput = document.getElementById('nameInput');
      const purposeInput = document.getElementById('purposeInput');
      const description = document.getElementById('descriptionInput');
      const urgencySelect = document.getElementById('urgencySelect');
      const statusSelect = document.getElementById('statusSelect');
      
      doctorSelect.value = visit._doctor;
      nameInput.value = visit._name;
      purposeInput.value = visit._purpose;
      description.value = visit._description;
      urgencySelect.value = visit._urgency;
      statusSelect.value = visit._status;
      
      renderAdditionalFields(visit._doctor);

      if (visit._doctor === 'Cardiologist') {
        document.getElementById('pressureInput').value = visit._pressure || '';
        document.getElementById('bmiInput').value = visit._bmi || '';
        document.getElementById('heartDiseasesInput').value = visit._heartDiseases || '';
        document.getElementById('ageInput').value = visit._age || '';
      } else if (visit._doctor === 'Dentist') {
        document.getElementById('lastVisitInput').value = visit._lastVisit || '';
      } else if (visit._doctor === 'Therapist') {
        document.getElementById('ageInput').value = visit._age || '';
      }

    this.openModal();
  }
  }

  const loginButton = document.getElementById('loginButton');
  const emailInput = document.getElementById("emailInput");
  const passwordInput = document.getElementById("passwordInput");

  loginButton.addEventListener('click', () => {
    loginModal.openModal();
  });

  const loginModal = new Modal('loginModal', 'closeLoginModalButton');
  const submitLoginForm = document.getElementById('submitLoginForm');
  const newtoken = 'f00783b8-92d1-4618-a6c4-39ea6f0ea038';

  function getToken(email, password) {
    return fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email, password })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Помилка HTTP: ${response.status}`);
        }
        return response.text();
      });
  }

  submitLoginForm.addEventListener('click', () => {
    const email = String(emailInput.value);
    const password = String(passwordInput.value);

    getToken(email, password)
      .then(function (token) {
        if (token === newtoken) {
          localStorage.setItem('myToken', token);
          console.log('Токени співпадають. Доступ дозволено.');
          document.querySelector('.loginButton').style.display = 'none';
          document.querySelector('.createVisitButton').style.display = 'block';
          document.querySelector('.logoutButton').style.display = 'block';
          document.querySelector('.filter-form').style.display = 'grid';
          loginModal.closeModal();
          console.log(token);
          console.log(newtoken);
          updateVisitList(token);
        }
        else {
          const loginForm = document.getElementById('loginForm');
          const errorText = document.createElement('p');
          errorText.innerText = 'Невірний email або пароль. Доступ заборонено.';
          errorText.classList.add("text-error");
          errorText.style.color = 'red';
          loginForm.insertAdjacentHTML('afterEnd', errorText.outerHTML);
        }
      })
      .catch(function (error) {
        console.error('Виникла помилка:', error);
        const loginForm = document.getElementById('loginForm');
        const errorText = document.createElement('p');
        errorText.innerText = 'Токен не знайдено. Доступ заборонено.';
        errorText.classList.add("text-error");
        errorText.style.color = 'red';
        loginForm.insertAdjacentHTML('afterEnd', errorText.outerHTML);
      });
    console.log(email, password);
  });


  const createVisitModal = new Modal('createVisitModal', 'closeCreateVisitModalButton', 'updateVisitSubmitButton');
  document.querySelector('.createVisitButton').addEventListener('click', () => {
    createVisitSubmitButton.style.display = 'block';
    updateVisitSubmitButton.style.display = 'none';
    createVisitModal.openModal();
  });

  const doctorSelect = document.getElementById('doctorSelect');
  const additionalFieldsContainer = document.getElementById('additionalFieldsContainer');

  function renderAdditionalFields(selectedDoctor) {
    if (selectedDoctor === 'Cardiologist') {
      additionalFieldsContainer.innerHTML = `
      <label for="pressureInput">Тиск:</label>
      <input type="text" id="pressureInput" name="pressureInput">
      <label for="bmiInput">Індекс маси тіла:</label>
      <input type="text" id="bmiInput" name="bmiInput">
      <label for="heartDiseasesInput">Перенесені захворювання серцево-судинної системи:</label>
      <input type="text" id="heartDiseasesInput" name="heartDiseasesInput">
      <label for="ageInput">Вік:</label>
      <input type="text" id="ageInput" name="ageInput">
    `;
    } else if (selectedDoctor === 'Dentist') {
      additionalFieldsContainer.innerHTML = `
      <label for="lastVisitInput">Дата останнього відвідування:</label>
      <input type="date" id="lastVisitInput" name="lastVisitInput">
    `;
    } else if (selectedDoctor === 'Therapist') {
      additionalFieldsContainer.innerHTML = `
      <label for="ageInput">Вік:</label>
      <input type="text" id="ageInput" name="ageInput">
    `;
    }
  }

  doctorSelect.addEventListener('change', () => {
    const selectedDoctor = doctorSelect.value;
    renderAdditionalFields(selectedDoctor);
  });

  const createVisitSubmitButton = document.getElementById('createVisitSubmitButton');

  createVisitSubmitButton.addEventListener('click', () => {
    const name = document.getElementById('nameInput').value;
    const purpose = document.getElementById('purposeInput').value;
    const description = document.getElementById('descriptionInput').value;
    const urgency = document.getElementById('urgencySelect').value;
    const status = document.getElementById('statusSelect').value;

    const selectedDoctor = doctorSelect.value;

    let newVisit;

    if (selectedDoctor === 'Cardiologist') {
      const pressure = document.getElementById('pressureInput').value;
      const bmi = document.getElementById('bmiInput').value;
      const heartDiseases = document.getElementById('heartDiseasesInput').value;
      const age = document.getElementById('bmiInput').value;
      newVisit = new VisitCardiologist(name, purpose, description, urgency, status, pressure, bmi, heartDiseases, age);
    } else if (selectedDoctor === 'Dentist') {
      const lastVisit = document.getElementById('lastVisitInput').value;
      newVisit = new VisitDentist(name, purpose, description, urgency, status, lastVisit);
    } else if (selectedDoctor === 'Therapist') {
      const age = document.getElementById('ageInput').value;
      newVisit = new VisitTherapist(name, purpose, description, urgency, status, age);
    }

    if (!name || !selectedDoctor || !purpose || !description || !urgency || !status) {
      alert('Будь ласка, заповніть всі поля.');
      return;
    }

    getToken("voidray@gmail.com", "01011970")
      .then(function (token) {
        createVisit(newVisit, token)
          .then(function (createdVisit) {
            createVisitModal.closeModal();
            console.log(createdVisit);
            updateVisitList(token);
          });
      });
  });


  function updateVisitList(token) {
    getVisits(token)
      .then(function (visits) {
        const accordionCard = document.getElementById('accordionCard');
        accordionCard.innerHTML = '';

        if (visits.length === 0) {
          console.log('No visits available.');
          document.querySelector('.start-text').style.display = 'block';
          return;
        }


        
        visits.forEach(function (visitData) {


          let visit = {...visitData}

          createVisitElement(visit);

          const deleteButton = accordionCard.querySelector('.delete-visit');
          console.log('Visit:', visitData);


        });

        document.querySelector('.start-text').style.display = 'none';
      });
  }

  function deleteVisitHandler(token, visitId) {
    console.log(visitId);
    deleteVisit(visitId)
      .then(() => {
        updateVisitList(token);
      })
      .catch(error => {
        console.error('Error deleting visit:', error);
      });
  };


  function deleteVisit(visitId) {
    const token = localStorage.getItem('myToken');
    if (!token) {
      console.error('Токен відсутній. Доступ заборонений.');
      return;
    }
  
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${visitId}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Помилка при видаленні візиту: ${response.statusText}`);
        }
      })
      .then(() => {
        console.log("Візит успішно видалено.");
        return Promise.resolve();
      })
      .catch(error => {
        console.error(error.message || error);
        return Promise.reject(error);
      });
  }


  function createVisitElement(visit) {
    const token = localStorage.getItem('myToken');
    const accordionCard = document.getElementById('accordionCard');
    const accordionCardItem = document.createElement('div');
    accordionCardItem.classList.add('accordion-item');
    accordionCardItem.setAttribute('id', visit.id);
    accordionCard.appendChild(accordionCardItem);

    const headingId = `panelsStayOpen-heading-${visit.id}`;
    
    const accordionHeader = document.createElement('h2');
    accordionHeader.classList.add('accordion-header');
    accordionHeader.id = headingId;
    accordionCardItem.appendChild(accordionHeader);

    const accordionButton = document.createElement('button');
    accordionButton.classList.add('accordion-button');
    accordionButton.setAttribute('type', 'button');
    accordionButton.setAttribute('data-bs-toggle', 'collapse');
    accordionButton.setAttribute('data-bs-target', `#panelsStayOpen-collapse-${visit.id}`);
    accordionButton.setAttribute('aria-expanded', 'false');
    accordionButton.setAttribute('aria-controls', `panelsStayOpen-collapse-${visit.id}`);
    accordionButton.innerText = visit._name;
    accordionHeader.appendChild(accordionButton);

    const collapseId = `panelsStayOpen-collapse-${visit.id}`;
    const accordionCollapse  = document.createElement('div');
    accordionCollapse .classList.add('accordion-collapse', 'collapse');
    accordionCollapse .setAttribute('id', collapseId);
    accordionCollapse .setAttribute('aria-labelledby', headingId);
    accordionCardItem.appendChild(accordionCollapse);

    const visitElement = document.createElement('div');
    visitElement.classList.add('accordion-body');
    accordionCollapse.appendChild(visitElement);

    const basicInfo = document.createElement('div');
    basicInfo.innerHTML = `
    <p>ПІБ: ${visit._name}</p>
    <p>Доктор: ${visit._doctor}</p>
  `;

    visitElement.appendChild(basicInfo);

    infoButton(visitElement, visit);
    
    const deleteButton = document.createElement('button');
    deleteButton.classList.add('delete-visit');
    deleteButton.innerHTML = '❌';
    deleteButton.setAttribute('id', visit.id)
    deleteButton.addEventListener('click', () => {
      deleteVisitHandler(token, visit.id);
      updateVisitList(token);
    });

    visitElement.prepend(deleteButton);
    return visitElement;
  }
  

  function infoButton(visitElement, visit) {
    const token = localStorage.getItem('myToken');
    const infoButton = document.createElement('button');
    infoButton.classList.add('info-button');
    infoButton.innerText = 'Показати більше';
    visitElement.appendChild(infoButton);

    const additionalInfo = document.createElement('div');
    

    infoButton.addEventListener('click', () => {
      additionalInfo.innerHTML = '';

      if (visit._doctor === 'Cardiologist') {
        additionalInfo.innerHTML = `
      <p>Мета візиту: ${visit._purpose}</p>
      <p>Короткий опис візиту: ${visit._description}</p>
      <p>Терміновість візиту: ${visit._urgency}</p>
      <p>Статус візиту: ${visit._status}</p>
      <p>Тиск: ${visit._pressure || 'N/A'}</p>
      <p>Індекс маси тіла: ${visit._bmi || 'N/A'}</p>
      <p>Перенесені захворювання серцево-судинної системи: ${visit._heartDiseases || 'N/A'}</p>
      <p>Вік: ${visit._age || 'N/A'}</p>
    `;
      } else if (visit._doctor === 'Dentist') {
        additionalInfo.innerHTML = `
      <p>Мета візиту: ${visit._purpose}</p>
      <p>Короткий опис візиту: ${visit._description}</p>
      <p>Терміновість візиту: ${visit._urgency}</p>
      <p>Статус візиту: ${visit._status}</p>
      <p>Останній візит: ${visit._lastVisit || 'N/A'}</p>
    `;
      } else if (visit._doctor === 'Therapist') {
        additionalInfo.innerHTML = `
      <p>Мета візиту: ${visit._purpose}</p>
      <p>Короткий опис візиту: ${visit._description}</p>
      <p>Терміновість візиту: ${visit._urgency}</p>
      <p>Статус візиту: ${visit._status}</p>
      <p>Вік: ${visit._age || 'N/A'}</p>
    `;
      }

      visitElement.appendChild(additionalInfo);
      infoButton.style.display = 'none';
      const editButton = document.createElement('button');
      editButton.classList.add('edit-button');
      editButton.innerText = 'Редагувати';
      visitElement.appendChild(editButton);
      let updateVisitSubmitButton = document.getElementById('updateVisitSubmitButton');

      editButton.addEventListener('click', () => {
        createVisitSubmitButton.style.display = 'none';
        updateVisitSubmitButton.style.display = 'block';
        createVisitModal.openEditModal(visit);
        updateVisitSubmitButton.addEventListener('click', () => {
          const name = document.getElementById('nameInput').value;
          const purpose = document.getElementById('purposeInput').value;
          const description = document.getElementById('descriptionInput').value;
          const urgency = document.getElementById('urgencySelect').value;
          const status = document.getElementById('statusSelect').value;
          const selectedDoctor = doctorSelect.value;
          
          
          let newVisit;

          if (selectedDoctor === 'Cardiologist') {
            const pressure = document.getElementById('pressureInput').value;
            const bmi = document.getElementById('bmiInput').value;
            const heartDiseases = document.getElementById('heartDiseasesInput').value;
            const age = document.getElementById('bmiInput').value;
            newVisit = new VisitCardiologist(name, purpose, description, urgency, status, pressure, bmi, heartDiseases, age);
          } else if (selectedDoctor === 'Dentist') {
            const lastVisit = document.getElementById('lastVisitInput').value;
            newVisit = new VisitDentist(name, purpose, description, urgency, status, lastVisit);
          } else if (selectedDoctor === 'Therapist') {
            const age = document.getElementById('ageInput').value;
            newVisit = new VisitTherapist(name, purpose, description, urgency, status, age);
          }

          if (!name || !selectedDoctor || !purpose || !description || !urgency || !status) {
            alert('Будь ласка, заповніть всі поля.');
            return;
          }

          updateVisit(newVisit,token, visit.id).then(() => {
            console.log(newVisit);
            createVisitModal.closeModal();
            updateVisitList(token);
            visit.id = ''
          })
          .catch(error => {
            console.error('Error deleting visit:', error);
          });
        })

      });
    });
  }


function getVisits(token) {
  return fetch('https://ajax.test-danit.com/api/v2/cards', {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.json());
}


function updateVisit(visitData, token, cardId){
  return fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(visitData)
  })
    .then(response => response.json())
}

function createVisit(visitData, token) {
  return fetch('https://ajax.test-danit.com/api/v2/cards', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(visitData)
  })
    .then(response => response.json());
}

function formFunc() {

  const formData = new FormData(document.querySelector('#filter'));
  const formCrit = Object.fromEntries(formData.entries());

  const token = localStorage.getItem('myToken');

  getVisits(token)
    .then((visits) => {
      const filteredVisits = visits.filter((visit) => {
        if (formCrit._name && formCrit._name.trim() !== '') {
          if (!visit._name.toLowerCase().includes(formCrit._name.trim().toLowerCase())) {
            return false;
          }
        }
        for (const critKey in formCrit) {
          if (critKey === '_name') continue;
          if (visit[critKey] !== formCrit[critKey]) {
            return false;
          }
        }
        return true;
      });

      const accordionCard = document.getElementById('accordionCard');
      accordionCard.innerHTML = '';

      if (filteredVisits.length === 0) {
        const startText = document.querySelector('.start-text');
        startText.style.display = 'block';
        startText.innerText = 'Немає результатів, що відповідають вибраним критеріям.';
        return;
      }

      filteredVisits.forEach((visitData) => {
        createVisitElement(visitData);
      });

      document.querySelector('.start-text').style.display = 'none';
    });

}
let formButton = document.getElementById('form-button');
formButton.addEventListener('click', (event)=> {
  event.preventDefault();
  formFunc()
});

});
